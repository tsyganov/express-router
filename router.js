var Router = function() {};

Router.prototype.setRoutes = function(routes) {
    this._routes = routes;
};

Router.prototype.setApp = function(app) {
    this._app = app;
};

Router.prototype.setContext = function(context) {
    this._context = context;
};

Router.prototype.setLogger = function(logger) {
    this._logger = logger;
};

Router.prototype.attachRoutes = function() {
    for (var name in this._routes) {
        if (this._routes.hasOwnProperty(name)) {
            this._attachSingleRoute(name);
        }
    }
};


Router.prototype.reverse = function (name, parameters) {
    var route = this._routes[name];

    if (!route) {
        throw new Error("Router error: route '" + name + "' does not exist");
    }

    var path = route["path"];
    for (var parameterKey in parameters) {
        if (parameters.hasOwnProperty(parameterKey)) {
            path = path.replace(":" + parameterKey, parameters[parameterKey]);
        }
    }

    return path;
};

Router.prototype._attachSingleRoute = function(name) {
    var route = this._routes[name];

    if (this._routeIsCorrect(route)) {
        var actionToCall = this._getActionToCall(route);
        this._app[route.httpMethod](route.path, actionToCall);
    }
    else {
        throw new Error("Router error: route " + name + " is incorrectly formed");
    }
};

Router.prototype._routeIsCorrect = function(route) {
    try {
        return typeof this._context[route.controller][route.method] == "function";
    } catch (e) {
        if (e instanceof TypeError) {
            return false;
        }
    }
};

Router.prototype._getActionToCall = function(route) {
    return function() {
        var args = Array.prototype.slice.call(arguments);
        this._context[route.controller][route.method].apply(this._context[route.controller], args);
    }.bind(this);
};

module.exports = Router;