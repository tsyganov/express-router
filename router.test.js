var assert = require("assert");
var chai = require("chai");
var sinon = require("sinon");
var sinonChai = require("sinon-chai");

chai.use(sinonChai);

var should = chai.should();

var demoRoutes = {
    'homepage': {
        httpMethod: 'get',
        path: '/',
        controller: 'index-controller',
        method: 'index'
    },
    'login': {
        httpMethod: 'post',
        path: '/login',
        controller: 'login-controller',
        method: 'login'
    },
    'action': {
        httpMethod: 'get',
        path: '/action/:parameter',
        controller: 'some-controller',
        method: 'action'
    }
};

var demoErrorRoutes = {
    'homepage': {
        httpMethod: 'get',
        path: '/',
        controller: 'not-existing-controller',
        method: 'index'
    }
};

var IndexController = function() {};
IndexController.prototype.someMethod = function() {};
IndexController.prototype.index = function() {
    this.someMethod();
};

var demoContext = {
    'index-controller': new IndexController(),
    'login-controller': {
        'login': function() {}
    },
    'some-controller': {
        'action': function(parameter) {}
    }
};

describe('express router', function() {
    var Router = require('./router');
    it('can be instantiated', function () {
        var router = new Router();
    });
    it('it has setters for dependencies', function() {
        var router = new Router();
        router.should.have.property('setRoutes');
        router.should.have.property('setApp');
        router.should.have.property('setContext');
        router.should.have.property('setLogger');
        router.should.have.property('reverse');
    });
    it('it should have attachRoutes method', function() {
        var router = new Router();
        router.should.have.property('attachRoutes');
    });
    describe('#.attachRoutes', function() {
        var router;
        var app;

        beforeEach(function () {
            router = new Router();

            app = function() {
                this._arguments = {
                    get: [],
                    post: []
                };
            };

            app.prototype.get = function(path, action) {
                this._arguments.get.push(arguments);
            };
            app.prototype.post = function(path, action) {
                this._arguments.post.push(arguments);
            };

            app = new app();

            router.setApp(app);
            router.setContext(demoContext);
        });

        it("should attach routes to app and every action call retains controller object context", function() {
            router.setRoutes(demoRoutes);
            router.attachRoutes();

            assert.equal(app._arguments.get[0][0], "/");
            assert.equal(app._arguments.get[1][0], "/action/:parameter");
            assert.equal(app._arguments.post[0][0], "/login");

            sinon.spy(demoContext['index-controller'], "index");
            sinon.spy(demoContext['index-controller'], "someMethod");
            sinon.spy(demoContext['login-controller'], "login");

            app._arguments.get[0][1]("test1", "test2");
            app._arguments.post[0][1]("test3", "test4");

            demoContext['index-controller'].index.should.be.calledWith("test1", "test2");
            demoContext['index-controller'].someMethod.should.be.called;
            demoContext['login-controller'].login.should.be.calledWith("test3", "test4");
        });

        it('should throw an error if something is wrong in route definition', function() {
            router.setRoutes(demoErrorRoutes);
            chai.expect(function() {router.attachRoutes();}).to.throw(Error);
        });

    });

    describe("#.reverse", function() {
        var router = new Router();

        router.setRoutes(demoRoutes);

        it("should return path for the login route", function() {
            router.reverse("login").should.be.equal("/login");
        });

        it("should return path for the 'action' route with parameter set", function() {
            router.reverse("action", {parameter: "52"}).should.be.equal("/action/52");
        });

        it("should return error if route does not exist", function () {
            (function(){
                router.reverse("route-does-not-exist");
            }).should.throw("Router error: route 'route-does-not-exist' does not exist");
        });
    });
});